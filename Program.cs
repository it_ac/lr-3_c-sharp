﻿using System;

namespace lr3test
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomArray();
            Console.WriteLine("Масив с случайними числами\n");
            
            EvenArray();
            Console.WriteLine("Масив с четными числами\n");
            
            OddArray();
            Console.WriteLine("Масив с нечетными числами\n");
           
            RandomSymbolArray();
            Console.WriteLine("Масив с случайними символами\n");
           
            RandomSymbolEnglishArray();
            Console.WriteLine("Масив с случайними буквами англ. алфавита\n");
           
            MaxArray();
            Console.WriteLine(" \n");
            
            MinArray();
            Console.WriteLine(" \n");
           
            CheckArray();
            Console.WriteLine(" \n");
            

            Console.ReadKey();
        }
        // Вывод масива


        // Заполнение области массива псевдослучайными числами.
        static int[,] RandomArray()
        {
            int[,] arr = new int[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    arr[i, j] = rand.Next(-100, 100);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            return arr;
        }

        // Заполнение области массива Четными/нечетными числами в диапазоне от n до m.
        static int[,] EvenArray()
        {
            int[,] arr = new int[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    do
                    {
                        arr[i, j] = rand.Next(-100, 100);
                    } while (arr[i, j] % 2 != 0);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            return arr;
        }

        static int[,] OddArray()
        {
            int[,] arr = new int[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    do
                    {
                        arr[i, j] = rand.Next(-100, 100);
                    } while (arr[i, j] % 2 == 0);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            return arr;
        }

        // Заполнение области массива Любыми символами.
        static char[,] RandomSymbolArray()
        {
            char[,] arr = new char[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    arr[i, j] = (char)rand.Next(0, 100);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            return arr;
        }

        // Заполнение области массива Любыми буквами англ. алфавита.
        static char[,] RandomSymbolEnglishArray()
        {
            char[,] arr = new char[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    arr[i, j] = (char)rand.Next('A', 'z');
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            return arr;
        }

        // Поиск в области массива max/min.
        static int[,] MaxArray()
        {
            int max = -100;
            int[,] arr = new int[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    arr[i, j] = rand.Next(-100, 100);
                    max = Math.Max(max, arr[i, j]);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            Console.Write($"Максимальное число масива {max} \n");
            return arr;
        }

        static int[,] MinArray()
        {
            int min = 100;
            int[,] arr = new int[3, 3];
            Random rand = new Random();
            int rows = arr.GetUpperBound(0) + 1;
            int columns = arr.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    arr[i, j] = rand.Next(-100, 100);
                    min = Math.Min(min, arr[i, j]);
                    Console.Write($"{arr[i, j]} \t");
                }
                Console.WriteLine();
            }
            Console.Write($"Минимальное число масива {min} \n");
            return arr;
        }

        // Поиск в области массива Заданного символа.
        static char[,] CheckArray()
        {


            int n = 3;
            char[,] Matrix = new char[n, n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Matrix[i, j] = (char)rand.Next('A', 'z');
                    Console.Write(Matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            char symbol = char.Parse(Console.ReadLine());
            int count = 0;
            string check = " ";
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (Matrix[i, j] == symbol)
                    {
                        check = "есть";
                        count++;
                    }
                }
            }
            Console.Write($"Символ {symbol} {check} \t");
            Console.WriteLine("count = {0}", count);
            return Matrix;
        }


        // Транспонировать массив.

        // Переписать элементы из одной области в другую.
    }
}
